const navbarToggler = document.querySelector('.navbar-toggler');
const navbar = document.querySelector('.navbar-presto')

navbarToggler.addEventListener('click', () => {
    navbarToggler.children[0].classList.toggle('fa-rotate-90')
})

document.addEventListener('scroll' , () => {
    let windowHeight = window.innerHeight;
    let scroll = window.pageYOffset

    if(scroll > windowHeight + 100){
        navbar.classList.add('mx-0')
        navbar.classList.add('px-5')
    } else {
        navbar.classList.remove('mx-0')
        navbar.classList.remove('px-0')
    }
})


let counters = document.querySelectorAll('.counter')

counters.forEach(counter => {
    let limit = counter.getAttribute('data-counter')
    let count = 0;
    let time = 6000 / limit;

    let interval = setInterval(() => {
        if(count <= limit){
            counter.innerHTML = count;
            count++
        } else {
            clearInterval(interval)
        }
    } , time)

})


const swiper = new Swiper('.swiper', {
    // Optional parameters
    // direction: 'vertical',
    loop: true,
    slidesPerView: 1,
    spaceBetween: 10,
    centeredSlides: true,
    autoplay: {
        delay: 2000,
        disableOnInteraction: false,
        pauseOnMouseEnter : true
    },
    // If we need pagination
    pagination: {
      el: '.swiper-pagination',
      dynamicBullets: true,
    },
  
    // Navigation arrows
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },

    breakpoints: {
        540: {
          slidesPerView: 2,
          spaceBetween: 20,
        },
        1024: {
          slidesPerView: 3,
          spaceBetween: 40,
        },
        1400: {
          slidesPerView: 4,
          spaceBetween: 50,
        },
    },
  

  });
